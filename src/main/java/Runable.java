import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;

public class Runable {
    public static void main(String[] args) throws DataFormatException {
       TmResultStore store = new TmResultStore("myDb");

        store.put(new TrafficResult("id2", 45L, 55L, Arrays.asList(4.3d, 5d, 9.9)));
        Result a = store.get("id2");
        List<Result> b = store.get(49L);

        ODPair o1 = new ODPair(1, 2, 5.5);
        ODPair o2 = new ODPair(4, 5, 4.8);
        ODPair o3 = new ODPair(3, 6, 3.2);
        store.put(new MatrixResult("id6", 40L, 50L, Arrays.asList(o1,o2,o3),
                Map.ofEntries(
                        Map.entry(9, Map.ofEntries(Map.entry(7,o1),Map.entry(4,o2))),
                        Map.entry(6, Map.ofEntries(Map.entry(3,o3),Map.entry(2,o2)))
                )));
        Result c = store.get("id6");

        store.remove("id2");
        store.remove("id6");
    }

}
