import org.bson.BsonDocument;
import org.bson.BsonDouble;
import org.bson.BsonInt32;

import java.io.Serializable;

public class ODPair implements Serializable {
    Integer from;
    Integer to;
    Double flow;

    private static final String FROM = "from";
    private static final String TO = "to";
    private static final String FLOW = "flow";

    public ODPair(int from, int to, double flow){
        this.from = from;
        this.to = to;
        this.flow = flow;
    }

    public static ODPair CreateFromDocument(BsonDocument document){
        int fromValue = document.get(FROM).asInt32().getValue();
        int toValue = document.get(TO).asInt32().getValue();
        double flowValue = document.get(FLOW).asDouble().getValue();
        return new ODPair(fromValue, toValue, flowValue);
    }

    public BsonDocument OutputBson() {
        return new BsonDocument()
                .append(FROM, new BsonInt32(from))
                .append(TO, new BsonInt32(to))
                .append(FLOW, new BsonDouble(flow));
    }
}
