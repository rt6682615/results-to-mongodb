
import java.util.*;
import java.util.zip.DataFormatException;

import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.BsonDocument;
import org.bson.BsonInt64;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jetbrains.annotations.NotNull;


/**
 * Result cache based on MapDB
 */
public class TmResultStore {

    private final MongoDatabase database;
    private final MongoCollection<BsonDocument> collection;
    private final MongoClient mongoClient;

    // connection string iri
    private static final String uri  = "mongodb://localhost:27017/";

    // should be modified
    private static final String collectionName = "myCollection";

    /**
     * @param filename the name for file database
     */
    public TmResultStore(String filename) {
        // Construct a ServerApi instance using the ServerApi.builder() method
        ServerApi serverApi = ServerApi.builder()
                .version(ServerApiVersion.V1)
                .build();
        MongoClientSettings settings;
        settings = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(uri))
                .serverApi(serverApi)
                .build();
        // Create a new client and connect to the server

            mongoClient = MongoClients.create(settings);
            database = mongoClient.getDatabase(filename);

/*
            Bson command = new BsonDocument("ping", new BsonInt64(1));
            Document commandResult = database.runCommand(command);
            System.out.println("Pinged your deployment. You successfully connected to MongoDB!");

*/
            collection = database.getCollection(collectionName, BsonDocument.class);

    }

    /**
     * @param result result
     */
    public void put(@NotNull Result result){
        collection.insertOne(result.OutputBson());
    }

    public Result get(String jobId) throws DataFormatException {
        Document query = new Document().append("_id", jobId);

        return Result.CreateFromDocument(collection.find(query).first());
    }

    public List<Result> get(Long time) throws RuntimeException {
        Bson filter = Filters.and(Filters.lte(Result.START_TIME, time), Filters.gte(Result.END_TIME, time));
        List<Result> results = new ArrayList<>();
        collection.find(filter).forEach(x -> {
            try {
                results.add(Result.CreateFromDocument(x));
            } catch (DataFormatException e) {
                throw new RuntimeException(e);
            }
        });
        return results;
    }

    public void remove(String jobId){
        Document query = new Document().append("_id", jobId);

        collection.findOneAndDelete(query);
    }

}

