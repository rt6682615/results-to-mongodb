
import org.bson.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.DataFormatException;

abstract class Result {
    String jobId;
    Long startTime;
    Long endTime;
    String type;

    public enum resultTypes {traffic, matrix}

    protected static final String TYPE = "type";
    protected static final String ID = "_id";
    protected static final String START_TIME = "startTime";
    protected static final String END_TIME = "endTime";
    public Result(String jobId, Long startTime, Long endTime, String type) {
        this.jobId = jobId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.type = type;
    }

    public static Result CreateFromDocument(BsonDocument document) throws DataFormatException {
        BsonValue typeObject = document.get(TYPE);
        if(typeObject == null){
            throw new DataFormatException("Result does not specifies its type.");
        }
        String type = typeObject.asString().getValue();
        if (type.equals(resultTypes.traffic.name())) {
            return TrafficResult.CreateFromDocument(document);
        } else if (type.equals(resultTypes.matrix.name())) {
            return MatrixResult.CreateFromDocument(document);
        }
        /* some more types */
        else throw new DataFormatException("Unknown result type.");
    }

    public abstract BsonDocument OutputBson();
}

class TrafficResult extends Result {

    List<Double> traffic;
    private static final String TRAFFIC = "traffic";
    private static final String NAME = TRAFFIC;

    public TrafficResult(String jobId, Long startTime, Long endTime, List<Double> traffic) {
        super(jobId, startTime, endTime, NAME);
        this.traffic = traffic;
    }

    @Override
    public BsonDocument OutputBson() {
        return new BsonDocument()
                .append(ID, new BsonString(jobId))
                .append(START_TIME, new BsonInt64(startTime))
                .append(END_TIME, new BsonInt64(endTime))
                .append(TRAFFIC, new BsonArray(traffic.stream().map(BsonDouble::new).toList()))
                .append(TYPE, new BsonString(NAME));
    }

    public static Result CreateFromDocument(BsonDocument document) throws DataFormatException {
        try {
            String job_id = document.get(ID).asString().getValue();
            Long start_time = document.get(START_TIME).asInt64().getValue();
            Long end_time = document.get(END_TIME).asInt64().getValue();
            List<Double> _traffic = document.get(TRAFFIC).asArray().stream().map(x ->x.asDouble().getValue()).toList();
            return new TrafficResult(job_id, start_time, end_time, _traffic);
        } catch (NumberFormatException | ClassCastException e) {
            throw new DataFormatException(e.getMessage());
        }
    }
}

class MatrixResult extends Result {
    List<ODPair> matrix;
    Map<Integer, Map<Integer, ODPair>> index;
    private static final String MATRIX = "matrix";
    private static final String INDEX = "index";
    private static final String NAME = MATRIX;

    public MatrixResult(String jobId, Long startTime, Long endTime, List<ODPair> matrix, Map<Integer, Map<Integer, ODPair>> index) {
        super(jobId, startTime, endTime, NAME);
        this.matrix = matrix;
        this.index = index;
    }

    private BsonDocument OutputBsonIndex(){
        BsonDocument indexDocument = new BsonDocument();
        for (Map.Entry<Integer, Map<Integer, ODPair>> i:index.entrySet()) {
            BsonDocument innerMap = new BsonDocument();
            for (Map.Entry<Integer, ODPair> j:i.getValue().entrySet()) {
                innerMap.append(j.getKey().toString(), j.getValue().OutputBson());
            }
            indexDocument.append(i.getKey().toString(), innerMap);
        }
        return indexDocument;
    }

    private static Map<Integer, Map<Integer, ODPair>> LoadBsonIndex(BsonDocument document){
        return document.get(INDEX).asDocument().entrySet().stream()
                .collect(Collectors.toMap(
                        x -> Integer.parseInt(x.getKey()),
                        x->  x.getValue().asDocument().entrySet().stream()
                                .collect(Collectors.toMap(
                                        y -> Integer.parseInt(y.getKey()),
                                        y -> ODPair.CreateFromDocument(y.getValue().asDocument())))));
    }

    @Override
    public BsonDocument OutputBson() {
        return new BsonDocument()
                .append(ID, new BsonString(jobId))
                .append(START_TIME, new BsonInt64(startTime))
                .append(END_TIME, new BsonInt64(endTime))
                .append(MATRIX, new BsonArray(matrix.stream().map(ODPair::OutputBson).toList()))
                .append(INDEX, OutputBsonIndex())
                .append(TYPE, new BsonString(NAME));
    }
    public static Result CreateFromDocument(BsonDocument document) throws DataFormatException {
        try {
            String job_id = String.valueOf(document.get(ID).asString().getValue());
            Long start_time = document.get(START_TIME).asInt64().getValue();
            Long end_time = document.get(END_TIME).asInt64().getValue();
            List<ODPair> _matrix = document.get(MATRIX).asArray().stream().map(x->ODPair.CreateFromDocument(x.asDocument())).toList();
            Map<Integer, Map<Integer, ODPair>> _index = LoadBsonIndex(document);
            return new MatrixResult(job_id, start_time, end_time, _matrix, _index);
        } catch (NumberFormatException | ClassCastException e) {
            throw new DataFormatException(e.getMessage());
        }
    }
}